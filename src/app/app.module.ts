import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddAlunoComponent } from './components/add-aluno/add-aluno.component';
import { AlunosListComponent } from './components/alunos-list/alunos-list.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/*import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt-PT';
registerLocaleData(localePt);*/


@NgModule({
  declarations: [
    AppComponent,
    AddAlunoComponent,
    AlunosListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    //BsDatepickerModule.forRoot()
  ],
  //providers: [{ provide: LOCALE_ID, useValue: 'pt-PT'},],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
