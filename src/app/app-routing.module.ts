import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlunosListComponent } from './components/alunos-list/alunos-list.component';
import { AddAlunoComponent } from './components/add-aluno/add-aluno.component';

const routes: Routes = [

  { path: '', redirectTo: 'alunos', pathMatch: 'full' },
  { path: 'alunos', component: AlunosListComponent },
  { path: 'alunos/:id', component: AddAlunoComponent },
  { path: 'add', component: AddAlunoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
