import { Component, OnInit } from '@angular/core';
import { AlunosService } from 'src/app/services/alunos.service';

@Component({
  selector: 'app-alunos-list',
  templateUrl: './alunos-list.component.html',
  styleUrls: ['./alunos-list.component.css']
})
export class AlunosListComponent implements OnInit {

  alunos: any;
  currentAluno = null;
  currentIndex = -1;
  nome = '';

  constructor(private alunoService: AlunosService) { }

  ngOnInit(): void {
    this.retrieveAlunos();
  }

  retrieveAlunos(): void {
    this.alunoService.getAll()
      .subscribe(
        data => {
          this.alunos = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  
  refreshList(): void {
    this.retrieveAlunos();
    this.currentAluno = null;
    this.currentIndex = -1;
  }

  setActiveAluno(aluno: any, index: number): void {
    this.currentAluno = aluno;
    this.currentIndex = index;
  }

}
