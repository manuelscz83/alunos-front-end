import { Component, OnInit } from '@angular/core';
import { AlunosService } from 'src/app/services/alunos.service';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { formatDate } from "@angular/common";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-aluno',
  templateUrl: './add-aluno.component.html',
  styleUrls: ['./add-aluno.component.css']
})
export class AddAlunoComponent implements OnInit {

  constructor(private alunoService: AlunosService, private route: ActivatedRoute,
    private router: Router) { 
  }

  ngOnInit(): void {
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    console.log(id);
    if (!Number.isNaN(id))
      this.getAluno(id);
  }
  getAluno(id: number): void {
    this.alunoService.get(id)
      .subscribe(
        data => {
          this.aluno = data;
          console.log(data);
          this.message = ""; 
        },
        error => {
          console.log(error);
          this.message = "Erro: " + error.error.error + ". Mensagem: " + error.error.message;
        });
  }

  frmAlunos = new FormGroup({
    primaryEmail: new FormControl('',[
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    nome: new FormControl('', Validators.required),
    cpf: new FormControl('',  Validators.required),
    sexo: new FormControl('',  Validators.required),
    data_NASCIMENTO: new FormControl('',[Validators.required,
      Validators.pattern("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)(?:0?2|(?:Feb))\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$")])
    });  
  get primEmail(){
      return this.frmAlunos.get('primaryEmail')
    }

  get getNome(){
    return this.frmAlunos.get('nome')
  }
  get getCpf(){
    return this.frmAlunos.get('cpf')
  }

  get getSexo(){
    return this.frmAlunos.get('sexo')
  }

  get getData_NASCIMENTO(){
    return this.frmAlunos.get('data_NASCIMENTO')
  }

  aluno = {
    id: null,
    nome: '',
    cpf: '',
    data_NASCIMENTO: '',
    sexo: '',
    EMAIL: '',
    DATA_CRIACAO: ''
  };

  submitted = false;

  message = '';

  onSubmit(data: any){

    console.log(this.aluno.id);
    if(this.aluno.id === undefined || this.aluno.id === null)
    {
      console.log("É novo aluno");
      //É novo aluno
      data = {
        id: this.aluno.id,
        nome: this.aluno.nome,
        cpf: this.aluno.cpf,
        data_NASCIMENTO: this.aluno.data_NASCIMENTO,
        sexo: this.aluno.sexo,
        EMAIL: this.aluno.EMAIL,
        DATA_CRIACAO: this.aluno.DATA_CRIACAO
      };
      this.alunoService.create(data)
        .subscribe(
          (response: any) => {
            console.log(response);
            this.submitted = true;
            this.message = 'O Aluno foi criado corretamente.';
          },
          (error: any) => {
            console.log(error);
            this.message = "Erro: " + error.error.error + ". Mensagem: " + error.error.message;
          });
    }
    else
    {
      console.log("É atualização");
      //É atualização
      data = {
        id: this.aluno.id,
        nome: this.aluno.nome,
        cpf: this.aluno.cpf,
        data_NASCIMENTO: this.aluno.data_NASCIMENTO,
        sexo: this.aluno.sexo,
        EMAIL: this.aluno.EMAIL,
        DATA_CRIACAO: this.aluno.DATA_CRIACAO
      };
      this.alunoService.update(this.aluno.id, data)
        .subscribe(
          response => {
            console.log(response);
            this.submitted = true;
            this.message = 'O Aluno foi atualizado corretamente.';
          },
          error => {
            console.log(error);
            this.message = "Erro: " + error.error.error + ". Mensagem: " + error.error.message;
          });
    }
  }

  newAluno(): void {
    this.submitted = false;
    this.aluno = {
      id: null,
      nome: '',
      cpf: '',
      data_NASCIMENTO: '',
      sexo: '',
      EMAIL: '',
      DATA_CRIACAO: ''
    };
    this.router.navigate(["/alunos/"]);
  }

 }
