import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl = 'http://localhost:8080/api/v1';

@Injectable({
  providedIn: 'root'
})
export class AlunosService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(`${baseUrl}/all`);
  }

  get(id: any): Observable<any> {
    return this.http.get(`${baseUrl}/find/${id}`);
  }
  
  create(data: any): Observable<any> {
    return this.http.post(`${baseUrl}/save`, data);
  };

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/save/${id}`, data);
  };

}
